<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('page.register');
    }

    public function submit(Request $request){
        $firstname = $request['firstnama'];
        $lastname = $request['lastnama'];        
        return view('welcome', compact('firstname', 'lastname'));
    }

}
