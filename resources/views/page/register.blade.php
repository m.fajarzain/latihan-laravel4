<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action ="/welcome" method="POST">
        @csrf
          
        <p>First Name</p>
        <input type="text" name="firstnama">
        
        <p>Last Name</p>
        <input type="text" name="lastnama">
        
        <p>Gender</p>
        <input type="radio"> Male <br>
        <input type="radio"> Female <br>
        <input type="radio"> Other <br>
        
        <p>Nationality</p>
        <select>
            <option value="Indonesian">Indonesian</option> 
        </select>

        <p>Language Spoken</p>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br>

        <p>Bio</p>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        
        <input type="submit" value="Daftar">
    </form>
</body>
</html>