@extends('layout.master')
@section('title')
    Tambah Cast
@endsection

@section('content')

<div>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Disini">
                @error('nama')
                    <div class="alert alert-danger">
                        Isi dulu namamu yaa !
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Title">
                @error('umur')
                    <div class="alert alert-danger">
                        Umur nya jangan lupa !
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Bio</label>
                <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Title">
                @error('bio')
                    <div class="alert alert-danger">
                        Begitu pun bio mu
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection